/*
package org.example.service;


import org.example.model.UserAccount;
import org.example.repository.UserAggregatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class UserAggregatorService implements UserDetailsService {

    @Autowired private UserAggregatorRepository repo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repo
                .findById(Integer.valueOf(username))
                .orElseThrow(()->new RuntimeException("No such user: "+username));
    }

}
*/
