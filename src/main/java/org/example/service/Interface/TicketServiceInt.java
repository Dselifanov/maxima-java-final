package org.example.service.Interface;

import org.example.model.Tickets;

import java.util.List;

public interface TicketServiceInt {

    void create(Tickets tickets);

    List<Tickets> readAll();

    Tickets read (int id);

    boolean update(Tickets tickets, int id);

    boolean delete(int id);
}
