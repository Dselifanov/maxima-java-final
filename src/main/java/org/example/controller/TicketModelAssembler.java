
package org.example.controller;

import org.example.model.Tickets;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class TicketModelAssembler implements RepresentationModelAssembler<Tickets, EntityModel<Tickets>> {


    @Override
    public EntityModel<Tickets> toModel(Tickets entity) {
        return EntityModel.of(entity,
                linkTo(methodOn(TicketController.class).read(entity.getId())).withSelfRel(),
                linkTo(methodOn(WebSiteController.class).readAll()).withRel("websites")
                );
    }

    @Override
    public CollectionModel<EntityModel<Tickets>> toCollectionModel(Iterable<? extends Tickets> entities) {
        return RepresentationModelAssembler.super.toCollectionModel(entities);
    }
}

