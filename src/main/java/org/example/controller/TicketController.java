package org.example.controller;

import org.example.model.Tickets;
import org.example.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/tickets")
public class TicketController {

    private final TicketService ticketService;
    @Autowired
    private TicketModelAssembler assembler;

    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }


    @PostMapping()
    public ResponseEntity<?> create(@RequestBody Tickets tickets) {
        ticketService.create(tickets);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping()
    public CollectionModel<EntityModel<Tickets>> readAll() {
        return assembler.toCollectionModel(ticketService.readAll());
    }


   /* @GetMapping()
    public ResponseEntity<List<Tickets>> readAll() {
        final List<Tickets> tickets = ticketService.readAll();

        return tickets != null &&  !tickets.isEmpty()
                ? new ResponseEntity<>(tickets, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }*/

    @GetMapping(value = "/{id}")
    public EntityModel<Tickets> read(@PathVariable(name = "id") int id) {
        final Tickets tickets = ticketService.read(id);

        return assembler.toModel(ticketService.read(id));

    }
    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@PathVariable(name = "id") int id, @RequestBody Tickets tickets) {
        final boolean updated = ticketService.update(tickets, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") int id) {
        final boolean deleted = ticketService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

}
