package org.example.model;

import jakarta.persistence.*;

@Entity
@Table(name = "Website")
public class WebSite {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @Column(name = "Name")
    private String name;

    @Column(name = "cost")
    private int costOfTicket;

    protected WebSite() {
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "tickets_id", referencedColumnName = "id")
    private Tickets tickets;


    public WebSite(String name, int costOfTicket) {
        this.name = name;
        this.costOfTicket = costOfTicket;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCostOfTicket() {
        return costOfTicket;
    }

    public void setCostOfTicket(int costOfTicket) {
        this.costOfTicket = costOfTicket;
    }

   public Tickets getTickets() {
        return tickets;
    }

    public void setTickets(Tickets tickets) {
        this.tickets = tickets;
    }

    @Override
    public String toString() {
        return "WebSite{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", costOfTicket=" + costOfTicket +
                '}';
    }
}

