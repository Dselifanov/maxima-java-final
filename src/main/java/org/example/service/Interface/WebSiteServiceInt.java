package org.example.service.Interface;


import org.example.model.WebSite;

import java.util.List;

public interface WebSiteServiceInt {

    void create(WebSite webSite);

    List<WebSite> readAll();

    WebSite read (int id);

    boolean update(WebSite webSite, int id);

    boolean delete(int id);
}
