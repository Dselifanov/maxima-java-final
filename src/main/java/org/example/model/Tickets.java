package org.example.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "Tickets")
public class Tickets {

    @Id
    @GeneratedValue
    private Integer id;


    private String cityName;


    private int date;

    private boolean isNeedVisa;

    protected Tickets() {
    }

    @OneToMany(mappedBy = "tickets", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<WebSite> webSite;

    public Tickets(String cityName, int date, boolean isNeedVisa) {
        this.cityName = cityName;
        this.date = date;
        this.isNeedVisa = isNeedVisa;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public boolean isNeedVisa() {
        return isNeedVisa;
    }

    public void setNeedVisa(boolean needVisa) {
        isNeedVisa = needVisa;
    }

    @Override
    public String toString() {
        return "Tickets{" +
                "id=" + id +
                ", cityName='" + cityName + '\'' +
                ", date=" + date +
                ", isNeedVisa=" + isNeedVisa +
                '}';
    }
}

