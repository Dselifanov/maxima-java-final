package org.example.service;

import jakarta.annotation.PostConstruct;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;

@Service
public class RSAKeys {
    private RSAPublicKey publicKey;

    @Value("${rsa.publickey.path}")
    private String publicKeyPath;

    @PostConstruct
    public void init() throws Exception {
        publicKey = readPublicKey();
    }

    public RSAPublicKey getPublicKey() {
        return publicKey;
    }

   private RSAPublicKey readPublicKey() throws Exception{
        String key = Files.readString(Path.of(publicKeyPath), Charset.defaultCharset());
        String publicKeyPEM = key
                .replace("-----BEGIN PUBLIC KEY-----","")
                .replace("-----END PUBLIC KEY-----","")
                .replace(System.lineSeparator(), "");
        byte[] encoded = Base64.decodeBase64(publicKeyPEM);

        KeyFactory keyFactory= KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
        return (RSAPublicKey) keyFactory.generatePublic(keySpec);
    }

}
