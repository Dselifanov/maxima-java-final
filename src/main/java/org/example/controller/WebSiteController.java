package org.example.controller;
import org.example.model.WebSite;
import org.example.service.WebSiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/websites")
public class WebSiteController {

    private final WebSiteService webSiteService;

    @Autowired
    public WebSiteController(WebSiteService webSiteService) {
        this.webSiteService = webSiteService;
    }


    @PostMapping()
    public ResponseEntity<?> create(@RequestBody WebSite webSite) {
        webSiteService.create(webSite);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity<List<WebSite>> readAll() {
        final List<WebSite> webSites = webSiteService.readAll();

        return webSites != null &&  !webSites.isEmpty()
                ? new ResponseEntity<>(webSites, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @GetMapping(value = "/{id}")
    public ResponseEntity<WebSite> read(@PathVariable(name = "id") int id) {
        final WebSite webSite = webSiteService.read(id);

        return webSite != null
                ? new ResponseEntity<>(webSite, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@PathVariable(name = "id") int id, @RequestBody WebSite webSite) {
        final boolean updated = webSiteService.update(webSite, id);

        return updated
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") int id) {
        final boolean deleted = webSiteService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }
}

