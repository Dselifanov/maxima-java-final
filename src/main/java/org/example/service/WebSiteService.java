
package org.example.service;
import org.example.model.WebSite;
import org.example.service.Interface.WebSiteServiceInt;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class WebSiteService implements WebSiteServiceInt {

    // Хранилище сайтов
    private static final Map<Integer, WebSite> WEB_SITE_REPOSITORY_MAP = new HashMap<>();

    // Переменная для генерации ID сайта
    private static final AtomicInteger WEB_SITE_ID_HOLDER = new AtomicInteger();

    @Override
    public void create(WebSite webSite) {
        final int webSiteId = WEB_SITE_ID_HOLDER.incrementAndGet();
        webSite.setId(webSiteId);
        WEB_SITE_REPOSITORY_MAP.put(webSiteId, webSite);
    }

    @Override
    public List<WebSite> readAll() {
        return new ArrayList<>(WEB_SITE_REPOSITORY_MAP.values());
    }

    @Override
    public WebSite read(int id) {
        return WEB_SITE_REPOSITORY_MAP.get(id);
    }

    @Override
    public boolean update(WebSite webSite, int id) {
        if (WEB_SITE_REPOSITORY_MAP.containsKey(id)){
            webSite.setId(id);
            WEB_SITE_REPOSITORY_MAP.put(id, webSite);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        return WEB_SITE_REPOSITORY_MAP.remove((id)) != null;
    }
}


