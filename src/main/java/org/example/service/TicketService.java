package org.example.service;

import org.example.model.Tickets;
import org.example.repository.TicketRepository;
import org.example.service.Interface.TicketServiceInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class TicketService implements TicketServiceInt {

    // Хранилище билетов
    private static final Map<Integer, Tickets> TICKET_REPOSITORY_MAP = new HashMap<>();

    // Переменная для генерации ID билета
    private static final AtomicInteger TICKET_ID_HOLDER = new AtomicInteger();

    @Autowired private TicketRepository ticketRepository;
    @Override
    public void create(Tickets tickets) {
        final int ticketsId = TICKET_ID_HOLDER.incrementAndGet();
        tickets.setId(ticketsId);
        TICKET_REPOSITORY_MAP.put(ticketsId, tickets);
    }

    @Override
    public List<Tickets> readAll() {
        return new ArrayList<>(TICKET_REPOSITORY_MAP.values());
    }

    @Override
    public Tickets read(int id) {
        return TICKET_REPOSITORY_MAP.get(id);

    }

    @Override
    public boolean update(Tickets tickets, int id) {
        if (TICKET_REPOSITORY_MAP.containsKey(id)){
            tickets.setId(id);
            TICKET_REPOSITORY_MAP.put(id, tickets);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        return TICKET_REPOSITORY_MAP.remove(id) != null;
    }
}
